# ImageRankingFuctionality

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Instuction to Run this project  
Download Node Js and install from  official Site
https://nodejs.org/en/download/

Download and Install Visul Studio Code From official Site
https://code.visualstudio.com/download

Step 1: Now go to the path of project in command Prompt (Ex. E:\Angular\project\itmcs>ng serve)

Step 2: Install the Angular CLI write commnad in Command prompt
	npm install -g @angular/cli (if you have not Install Angular)
	
Step 3: Write Commnad:	ng serve 

Step 4: Open Browser and hit this Url http://localhost:4200/